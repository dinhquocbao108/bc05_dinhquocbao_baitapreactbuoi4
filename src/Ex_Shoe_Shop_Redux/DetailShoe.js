import React, { Component } from 'react'
import { connect } from 'react-redux';

class DetailShoe extends Component {
  render() {
    let {name,price,description,shortDescription} = this.props.detail;
    return (
      <div className='row pt-5'>
        <img className='col-4' src={this.props.detail.image} alt="" />
        <div className='col-8'>
          <p>{name}</p>
          <p>{price}</p>
          <p>{description}</p>
          <p>{shortDescription}</p>
        </div>
      </div>
    )
  };
};
let mapStateToProps = (state)=>{
  return{ detail: state.shoeReducer.detail,
  };
}
export default connect(mapStateToProps)(DetailShoe);