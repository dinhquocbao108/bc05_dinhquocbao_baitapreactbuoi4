import React, { Component } from 'react'
import DetailShoe from './DetailShoe';
import ListShoe from './ListShoe';
import Cart from './Cart';

export default class Ex_Shoe_Shop extends Component {
  render() {
    return (
      <div className='container'>
        <Cart/>
        <ListShoe/>
        <DetailShoe/>
      </div>
    )
  }
}
