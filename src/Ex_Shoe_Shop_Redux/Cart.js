import React, { Component } from "react";
import { connect } from "react-redux";
import { MINUS_NUMBER, PLUS_NUMBER } from "./shoeConstant";

class Cart extends Component {
  renderTbody = () => {
    return this.props.dataCart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.number}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleMinusNumber(item );
              }}
            >
              -
            </button>
            {item.number}
            <button
              onClick={() => {
                this.props.handlePlusNumber(item);
              }}
            >
              +
            </button>
          </td>
          <td>
            <img
              style={{
                height: "80px",
              }}
              src={item.image}
              alt=""
            />
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>NAME</th>
              <th>PRICE</th>
              <th>QUANTITY</th>
              <th>IMAGE</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handlePlusNumber: (shoe) => {
      dispatch({
        type: PLUS_NUMBER,
        payload: shoe,
      });
    },
    handleMinusNumber: (shoe) => {
      dispatch({
        type: MINUS_NUMBER,
        payload: shoe,
      });
    },
  };
};
let mapStateToProps = (state) => {
  return { dataCart: state.shoeReducer.cart };
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
