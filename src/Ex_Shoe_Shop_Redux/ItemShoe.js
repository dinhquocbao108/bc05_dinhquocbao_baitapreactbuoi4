import React, { Component } from "react";
import { connect } from 'react-redux';
import { ADD_TO_CART, CHANGE_DETAIL } from "./shoeConstant";

class ItemShoe extends Component {
  render() {
    return (
      <div className="col-3 my-auto" >
        <div className="card text-left">
          <img className="card-img-top" src={this.props.data.image} alt />
          <div className="card-body">
            <h4 className="card-title">{this.props.data.name}</h4>
            <h4 className="card-title">{this.props.data.price}$</h4>
            <button onClick={()=>{
                this.props.handleAddToCart(this.props.data)
            }} className="btn btn-danger ">Buy</button>
            <button onClick={()=>{
                this.props.handleChangeDetail(this.props.data)
            }} className="btn btn-primary ml-5">Detail</button>
          </div>
        </div>
      </div>
    );
  }
}
let mapDispatchToProps=(dispatch)=>{
    return{
        handleChangeDetail:(shoe)=>{
            dispatch({
                type:CHANGE_DETAIL,
                payload:shoe,
            });
        },
        handleAddToCart:(shoe)=>{
            dispatch({
                type:ADD_TO_CART,
                payload:shoe,
            })
        }
    }
}

export default connect(null,mapDispatchToProps)(ItemShoe);
