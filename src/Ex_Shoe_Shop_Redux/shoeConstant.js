export const ADD_TO_CART = 'ADD_TO_CART';
export const CHANGE_DETAIL = 'CHANGE_DETAIL';
export const PLUS_NUMBER = 'PLUS_NUMBER';
export const MINUS_NUMBER = 'MINUS_NUMBER';