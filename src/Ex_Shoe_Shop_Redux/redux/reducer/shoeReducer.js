import { dataShoe } from "../../dataShoe";
import { CHANGE_DETAIL, MINUS_NUMBER, PLUS_NUMBER } from "../../shoeConstant";
import { ADD_TO_CART } from './../../shoeConstant';
const initialState = {
  shoeArr: dataShoe,
  detail: dataShoe[0],
  cart: [],
};
export const shoeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case CHANGE_DETAIL: {
      state.detail = payload;
      return { ...state };
    };
    case ADD_TO_CART:{
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item)=>{
        return item.id === payload.id;
      });
      if(index==-1){
        let cartItem = {...payload,number:1};
        cloneCart.push(cartItem);
      }else{
        cloneCart[index].number++;
      }
      return{...state,cart:cloneCart};
    };
    case PLUS_NUMBER:{
        let cartitem = [...state.cart];
        let index = cartitem.findIndex((item)=>item.id===payload.id);
        cartitem[index].number++;
        return{...state,cart:cartitem};
    };
    case MINUS_NUMBER:{
        let cartitem = [...state.cart];
        let index = cartitem.findIndex((item)=>item.id===payload.id);
        if(cartitem[index].number>=1){
            cartitem[index].number--;
        }
        if(cartitem[index].number==0){
            cartitem.splice(index,1);
        }
        return{...state,cart:cartitem};
        
    }
    default:
      return state;
  }
};
